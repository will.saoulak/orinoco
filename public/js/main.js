// ---------------------------------------- PAGE DU SITE
const PageSite = {
    current : null,
    currentId : null,
    // recupere la nom de la page actuel
    setCurrent : function() {
        this.current = null;
        let regex = /([a-z]{4,})(.html)$/;
        let matched = document.location.pathname.match(regex);
        if(matched && matched.length == 3){
            this.current = matched[1];
        }
        return;
    },
    // recherche id= dans l url . si trouver recupere l id actuel 
    setCurrentId : function() { 
        this.currentId = null;
        let regex = /(id)(\=)([a-z0-9]{1,})/i;
        let matched = document.location.search.match(regex);
        if(matched && matched.length === 4){
            this.currentId = matched[3];
        }
        return;
    },
    initialize : function(){
        this.setCurrent();
        this.setCurrentId();
    }
}
// ---------------------------------------- FIL D'ARIANE
const Ariane = {
    currentPage : null,
    currentCategorie : null,
    currentProductName : null,
    // chemin
    pathFinding : {
        index : [ 'index' , 'categorie'],
        product : [ 'index' , 'categorie' , 'product'],
        caddy : [ 'caddy' ],
        validate : [ 'caddy' , 'validate']
    },
    // definition du contenue de chaque adresse du chemin
    pathDef : {
        index : { url : './index.html' , title : 'la boutique'},
        caddy : { url : './caddy.html' , title : 'Mon panier'},
        validate : { url : './validate.html' , title : "Confirmation d'achat"},
        categorie : { url : './index.html' , title : 'categorie'},
        product :  { url : './product.html' , title : 'product'}
    },
    // initilisation
    initialize : function(currentPage , currentCategorie , currentProductName){
        this.currentPage = (currentPage) ? currentPage : null;
        this.currentCategorie = (currentCategorie) ? currentCategorie : null;
        this.currentProductName = (currentProductName) ? currentProductName : null;

        if( this.currentCategorie )  this.pathDef.categorie.title = this.currentCategorie;
        if( this.currentProductName ) this.pathDef.product.title = currentProductName;
        
        return;
    }
}
// ---------------------------------------- API
const MyApi =  {
    url : 'http://localhost:3000/api',
    categorie : 'cameras',
    // promise de requete sur produit
    setRequestLoadDatas : function( id ) {
        let urlRequest = this.url + '/' + this.categorie;
        if(id) urlRequest += '/' + id;
        return new Promise( (resolve , reject) =>{
            let xhr = new XMLHttpRequest();
            xhr.onload = function(){
                resolve( JSON.parse(this.response) );
            }
            xhr.onerror = function(){
                reject( 'erreur chargement des donnees sur '+urlRequest );
            }
            xhr.open('GET', urlRequest);
            xhr.send();
        });
    },
    // promise de requete envoie donnée payment
    setRequestOrder : function( contact , products){
        return new Promise( (resolve) =>{
            let request = new XMLHttpRequest();
            request.onload = function(){
                resolve(this.responseText);
            }
            request.open("POST" , this.url+'/'+this.categorie+'/order');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify({contact , products}));
        });
    }
}

// **************** ECRITURE DES ELEMNENTS DYNAMIQUES
// fil d'ariane
async function printAriane(){
    return new Promise( (resolve , reject) => {
        $(document).ready( function(){
            try{
                if(! Ariane.currentPage ){
                    reject( ['Erreur ariane','current path', Ariane.currentPath] );
                }
                let currentPath = Ariane.pathFinding[ Ariane.currentPage ];
                let n = currentPath.length - 1;
                $("#ariane").find('ol').children().remove();
        
                for(let i in currentPath ){
                    let def = Ariane.pathDef[ currentPath[i] ];
                    let node;
                    if( i == n ){ 
                        node = $("<li />", { class:'breadcrumb-item active' , 'aria-current':'page' }).text( def.title );
                    }
                    else{
                        node = $("<li />", { class:'breadcrumb-item'} ).append( $("<a />",{ href : def.url }).text( def.title ) );
                    }
                    $("#ariane").find('ol').append(node);
                }
            }catch( error ){
                reject(error);
            }
            resolve('fil d ariane succe');
        });
    });
}
// panier du menue
async function print_caddy_menu(caddy){
    return new Promise( (resolve,reject) => {
        let pageCurrent = PageSite.current;
        $(document).ready( function(){
            try{
                caddy.printHeader( $('.caddyMenue--header') );
                if( pageCurrent === 'index' || pageCurrent === 'product' ){
                    let nodeListe = $('.caddyMenue').find("[data-update=caddyList]");
                    let lineTemplate = $("#caddyMenueLine .caddyMenueLine");
                    let noArticleTemplate =  $("#noArticle p");
                    caddy.printBody( nodeListe ,lineTemplate , noArticleTemplate);
                }
            }
            catch( error ){
                reject( error );
            }
            resolve('caddy menue succes');  
        });
    }); 
}
// panier en detail
async function print_caddy_detail(caddy , formValidate){
    return new Promise( (resolve,reject) => {
        $(document).ready( function(){
            try{
                caddy.printHeader( $('.caddyDetail--header') );
                let nodeListe = $('.caddyDetail--body').find('[data-update=caddyList]');
                let lineTemplate = $("#caddyDetailLine .caddyDetailLine");
                let noArticleTemplate =  $("#noArticle p");
                caddy.printBody( nodeListe ,lineTemplate , noArticleTemplate);
                caddy.printFooter( $('.caddyDetail--footer') );

                if(caddy.products.length === 0){
                    formValidate.hide();
                }else{
                    formValidate.show();
                }
            }catch( error ){
                reject( error );
            }
            resolve('succes');
        });
    });  
}
// list de produit
async function print_product_list( parentNode ,  listDatas){
    return new Promise( (resolve , reject) => {
        $(document).ready( function(){
            try{
                $("#pageResult").find('[data-update=productCategorie]').text(MyApi.categorie);
                let nodeTemplate = $("#resultProductLine .productResult");
                let noArticleTemplate = $("#noArticle p");
                parentNode.children().remove();
                if( listDatas.length  === 0 ){  // pas d'article trouver + fin
                    parentNode.apppend( noArticleTemplate.clone() );
                }
                else{
                    for(let datas of listDatas){ // ecriture ligne par ligne 
                        let product = new Product(datas);
                        let node =  product.printDatas( nodeTemplate.clone() );
                        parentNode.append(node);
                    }
                }
            }catch( error ){
                reject( error );
            }
            resolve('liste produit succes');
        });
    });
  
}
// product detail
async function print_product_detail( parentNode , product){
    return new Promise( (resolve,reject) => {
        $(document).ready( function(){
            try{
                product.printDatas( parentNode );
            }catch( error ){
                reject( error );
            }
            resolve();
        });
    });  
}
// page de validation d achat
async function printPage_validate(caddyOrder){
    let page = $("#pageValidate");
    let list = page.find('[data-update=orderProductList]');
    
    page.find('[data-update=orderId]').text(caddyOrder.orderId);
    page.find('[data-update=orderAmount]').text( caddyOrder.getAmount( true , 'EUR') );
    page.find('[data-update=orderIdent]').text( caddyOrder.contact.firstName+' '+caddyOrder.contact.lastName );
    page.find('[data-update=orderAddress]').text( caddyOrder.contact.address+' '+caddyOrder.contact.city );

    let templateLine = $("#validateLine .validateLine");
    list.children().remove();
 
    for(let i in caddyOrder.products){ // ecriture ligne par ligne
        let node = caddyOrder.printLine(i , templateLine.clone() );
        list.append(node);
    }
}
// convertit la valeur centimes en euros
function convertDevise(amount , devise = null){
    let n = (amount / 100).toFixed(2);
    if(devise) n+= ' '+devise;
    return n;
}
// recupere les donnees saisie dans le formulair
function collectDataForm(form){
    let error = 0;
    let contact = {
        firstName : form.find("input[name='formBuyFirstName']").val(),
        lastName : form.find("input[name='formBuyLastName']").val(),
        city : form.find("input[name='formBuyCity']").val(),
        email : form.find("input[name='formBuyEmail']").val(),
        address : ""
    }
    form.find("input[name^='formBuyAddress']").each(function(i){
        if(contact.address.length > 0) contact.address+= ' ';
        contact.address += $(this).val();
    });
    for(let required in contact){
        if(! required || required.length === 0){
            error++;
            break;
        }
    }
    if(error === 0) return contact;
    else return null;  
}
// fonction main
async function main(){  
    PageSite.initialize();
    const caddyTemp  = new Caddy('tempory');
    const caddyOrder = new Caddy('order');
    const listDatasProd = new ListDatas(); 
    
    if( PageSite.current === 'index'){
        await listDatasProd.load(MyApi);
        Ariane.initialize(PageSite.current , MyApi.categorie);
    }
    else if(PageSite.current === 'product'){
        await listDatasProd.load(MyApi , PageSite.currentId);
        Ariane.initialize(PageSite.current , MyApi.categorie , listDatasProd.content[0].name );
    } 
    else{
        Ariane.initialize(PageSite.current);
    }

    switch(PageSite.current){
        case 'index':
            await printAriane();
            await print_caddy_menu(caddyTemp);
            await print_product_list( $("#pageResult").find('[data-update=productList]'), listDatasProd.content )
        break;
        case 'product' :
            const product = new Product(listDatasProd.content[0]);
            await printAriane();
            await print_caddy_menu(caddyTemp);
            await print_product_detail( $("#productDetail") , product ).then( () =>{
                Product._observeChangeOption( $("#productDetail").find('[data-update=productOption]') , product );
                Product._observeAddToCaddy( $("#productDetail").find('.productDetail--action button.addCaddy') , product , caddyTemp);
            });
        break;
        case 'caddy' :
            await printAriane();
            await print_caddy_menu(caddyTemp);
            await print_caddy_detail(caddyTemp , $("#caddyValidate"));
            $("#caddyValidate").on('submit' , function(event){
                event.stopPropagation();
                event.preventDefault();
                let form = event.target;
                let contact = collectDataForm( $(form) );
                let products = caddyTemp.getAllListId(); 
                MyApi.setRequestOrder(contact,products).then( (order) =>{
                    caddyOrder.initialize(JSON.parse(order));
                    caddyOrder.save();
                    caddyTemp.clear();
                    form.submit();
                });
            });
        break;
        case 'validate' : 
            await printAriane();
            await print_caddy_menu(caddyTemp);
            printPage_validate(caddyOrder);
        break; 
    }
}