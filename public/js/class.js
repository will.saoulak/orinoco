// ---------------------------------------- PRODUITS
class Product{
    constructor( datas ){
        this.datas = {
            _id  : datas._id,
            name : datas.name,
            option : datas.lenses || datas.colors || datas.varnish || [null],
            price : datas.price,
            description : datas.description,
            imageUrl : datas.imageUrl
        };
        this.currentOption = datas.currentOption || this.datas.option[0];
    }
    // ecriture des données du produit
    printDatas(jqNodeParent){
        // si jqNodeParent => string id || jqNode
        // recheche une class nommée product-[nom data] dans le noeud idParent puis la mais a jour
        // retourne jqNodeParent mis a jour
        if(typeof jqNodeParent === 'string') jqNodeParent = $("#"+jqNodeParent);
        
        jqNodeParent.find('[data-update=productName]').text(this.datas.name);
        jqNodeParent.find('[data-update=productPrice]').text( convertDevise(this.datas.price , 'EUR') );
        jqNodeParent.find('[data-update=productDescription]').text(this.datas.description);
        jqNodeParent.find('[data-update=productCurrentOption]').text(this.currentOption);
        jqNodeParent.find('[data-update=productImage]').attr({
            src : this.datas.imageUrl,
            alt : 'photos de '+this.datas.name
        });
        jqNodeParent.find('[data-update=productPageLink]').attr({
            href : 'product.html?id='+this.datas._id,
            title : 'voir '+this.datas.name
        });

        let selectOption = jqNodeParent.find('[data-update=productOption]');
        if(selectOption.length){
            selectOption.children().remove();
            for(let option of this.datas.option){
                let optNode = $('<option/>' , {value:option} );
                optNode.text(option);
                selectOption.append(optNode);
            }
        }

        return jqNodeParent;
    }
    // lists des observer 
    static _observeChangeOption(nodeToObserve , product){
        nodeToObserve.on('change' , function( event ){
            product.currentOption = $(event.target).val();
        });
    }
    static _observeAddToCaddy(nodeToObserve , product , caddy){
        nodeToObserve.on('click' , function( event ){
            event.stopPropagation();
            caddy.addProduct( product , 1);
        });
    }
    static _observeRemoveToCaddy(nodeToObserve , product , caddy){
        nodeToObserve.on('click' , function( event ){
            event.stopPropagation();
            caddy.removeProduct( product , 1);
        });
    }
}
// ---------------------------------------- PANNIER
class Caddy{
    /********************  admin session */
    constructor(nameStorage){
        this.nameStorage = nameStorage;
        this.initialize();
        this.retrieve();
    }
    initialize(init){ // initialisation propertys || reset propertys
        if(init){
            this.products = init.products;
            this.contact = init.contact;
            this.orderId = init.orderId;
        }
        else{
            this.orderId = null;
            this.products = [];
            this.contact = {
                firstName: null ,
                lastName : null,
                address : null
            }
        }
       
    }
    retrieve(){ // recuperation
        let content = sessionStorage.getItem( this.nameStorage );
        if(content) this.initialize( JSON.parse(content) );
    }
    save(){ // sauvegarde
        sessionStorage.setItem( this.nameStorage , JSON.stringify({ 
            products : this.products , 
            contact : this.contact ,
            orderId: this.orderId 
        }) );
    }
    clear(){ // supression
        sessionStorage.removeItem(this.nameStorage);
        this.initialize(null);
    }  
    /******************** filtre et recherche par ligne */
    getKey( idProduct , currentOptionProduct ){ //retourne  => la clef de products  par id et option || null
        return this.products.findIndex( (product) => {
            return (product._id === idProduct && product.currentOption === currentOptionProduct );
        });
    }
    getLine( idProduct , currentOptionProduct){ // retourne la ligne de articles par id et option || null
        let key = this.getKey(  idProduct , currentOptionProduct );
        if(key >= 0) return this.products[key];
        else return null;
    }
    getListOfIdSorted(){ // retourne une liste des id different || []
        let list = [];
        for(let product of this.products){
            //  id n est pas dans la list
            if( list.indexOf(line._id) < 0 ){
                list.push(line._id);
            }
        }
        return list;
    }
    getAllListId(){ // retourne une liste de tous les id  || []
        let list = [];
        for(let product of this.products){
            // ajoute une ligne par de produit
            for(let i = 0 ; i < product.qt ; i++){
                list.push(product._id);
            }
        }
        return list;
    }
    /******************** manipulation */
    addLine( id , name , currentOption , price , imageUrl , qt){ // creer une nouvelle ligne
        let obj = {
            name : name ,
            _id : id,
            currentOption : currentOption,
            price : price,
            qt : qt,
            imageUrl : imageUrl
        }
        this.products.push(obj);
    }
    addProduct(product , qt){ // ajoute un produit (Product)
        // recherche de produit existant
        let key = this.getKey( product.datas._id , product.currentOption);
        if( key >= 0){ // produit present modification de qt
            this.products[key].qt += qt;
        }
        else{ // non present creation nouvelle ligne
            this.addLine(product.datas._id , product.datas.name , product.currentOption , product.datas.price , product.datas.imageUrl , qt);
        }
        // sauvergarde modif
        this.save();
        // mise a jour ecran    
        print_caddy_menu(this);
        if( PageSite.current == 'caddy' ){
            print_caddy_detail(this , $("#caddyValidate") );
        }  
    }
    dellLine(key){ // suprime une ligne
        this.products.splice(key,1);
    }
    removeProduct( product , qt ){ // retire un article
        // recherche du produit a retirer
        let key = this.getKey( product.datas._id , product.currentOption);
        if( key >= 0){ // si trouver deduire qt
            this.products[key].qt -= qt;
            if( this.products[key].qt <= 0){ // si qt <= 0 suprimer ligne
                this.dellLine(key);
            }
        }
        // sauvegarde
        this.save();
         // mise a jour ecran    
        print_caddy_menu(this);
        if( PageSite.current == 'caddy' ){
            print_caddy_detail(this , $("#caddyValidate") );
        } 
    }
    /******************** calcule */
    getAmountLine( key , do_convert , devise = null){ // retourne le montant d'une ligne formater ou non (fct:convertDevise)
        if( this.products[key] ){
            if( ! this.products[key].qt ) this.products[key].qt = 1 ;
            let amount = this.products[key].price * this.products[key].qt;
            if( do_convert )  return convertDevise( amount , devise );
            else return amount;
        }
    }
    getAmount( do_convert , devise = null){ // retourne le montant total formater ou non (fct:convertDevise)
        let amount = 0;
        for(let i in this.products){
            amount += this.getAmountLine(i,false);
        }
        if( do_convert )  return convertDevise( amount , devise );
        else return amount;
    }
    getCount(){ // retourne le nombre total d article
        let count = 0;
        for(let product of this.products){
            count += product.qt;
        }
        return count;
    }
    /******************** ecriture */
    printLine(key , node){ // ecris le contenue d'une ligne cible dans node
        // si node est de type string recuperation neud html
        if(typeof node === 'string') node = $("#"+node);
        // ciblage ligne a ecrire
        let line = this.products[key];
        // si ligne non trouver fin
        if(! line ) return node;
        // mise a jour du noeud par non de données
        node.find('[data-update=caddyLineName]').text(line.name);
        node.find('[data-update=caddyLineCurrentOption]').text(line.currentOption);
        node.find('[data-update=caddyLinePrice]').text( convertDevise(line.price , 'EUR') );
        node.find('[data-update=caddyLineQt]').text( line.qt );
        node.find('[data-update=caddyLineAmount]').text( this.getAmountLine( key , true , 'EUR') );
        node.find('[data-update=caddyLinePageLink]').attr({href:'product.html?id='+line._id});
        node.find('[data-update=caddyLineImage]').attr({src : line.imageUrl});
        // si page caddy activer les observer ajout/supresion produit
        if( PageSite.current === 'caddy' ){
            let product = new Product( line );
            Product._observeAddToCaddy( node.find('[data-action=add]') , product , this );
            Product._observeRemoveToCaddy( node.find('[data-action=remove]') , product , this);
        }
        // retourne le noeud html a jour
        return node;  
    }
    printHeader(node){ // ecris le contenue entete dans node (nombre & montant tt)
        node.find('[data-update=caddyCount]').text( this.getCount() );
        node.find("[data-update=caddyAmount]").text( this.getAmount(true) );
    }
    printBody(nodeList , templateLine , noArticleTemplate){// ecris contenue du panier ligne par ligne dans nodeList
        // effacer contenue actuel
        nodeList.children().remove();

        // ecriture du contenue
        if( this.products.length  === 0 ){ // pas d'article + fin
            nodeList.append(noArticleTemplate.clone());
            return;
        }

        // ecriture ligne par ligne
        for(let key in this.products){
            let node = this.printLine(key , templateLine.clone()); // recuperation ligne template
            nodeList.append(node); // insertion
        }
        return nodeList;
    }
    printFooter(node){ // ecris pied du caddy dans node
        node.find("[data-update=caddyAmount]").text( this.getAmount(true , 'EUR') );
    }
}

// ---------------------------------------- LISTE DE DONNEES
class ListDatas{
    constructor(){
        this.content = [];
    }
    // retourn la cles de content de l'id trouver ou -1
    getKeyById( id ){
        return this.content.findIndex( (line) => {
            return (line._id === id);
        });
    }
    // retourn la ligne de l'id trouver ou NULL
    searchById( id ){
        let key = this.getKeyById( id );
        if(key >= 0) return this.content[key];
        else return null;
    }
    // ajoute un ou plusieur ligne a la liste
    addToList( datas ){
        if( Array.isArray(datas) ){
            for(let lineDatas of datas){
                this.addToList(lineDatas);
            }
            return ;
        }
        this.content.push(datas);
    }
    /**** PROMISES **/
    // charge le contenue par api . si id non fourni charge la liste complete de categorie   
    async load( api , id= null){
        if(! id) id = null;
        return api.setRequestLoadDatas(id).then( datasList => this.addToList(datasList) );
    }
}
